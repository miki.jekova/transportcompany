package entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.Set;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="company")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Min(value = 0L, message = "Revenue must be a positive number!")
    @Column(name = "revenue", nullable = true)
    private double revenue;

    @OneToMany(mappedBy = "company", targetEntity = Driver.class)
    private Set<Driver> drivers;

    @OneToMany(mappedBy = "company", targetEntity = Vehicle.class)
    private Set<Vehicle> vehicles;

    @ManyToMany(mappedBy = "companies", targetEntity = Client.class)
    private Set<Client> clients;

    @OneToMany(mappedBy = "company", targetEntity = Transportation.class)
    private Set<Transportation> transportations;

    public Company(String name, double revenue, Set<Driver> drivers, Set<Vehicle> vehicles, Set<Client> clients, Set<Transportation> transportations) {
        this.name = name;
        this.revenue = revenue;
        this.drivers = drivers;
        this.vehicles = vehicles;
        this.clients = clients;
        this.transportations = transportations;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", revenue=" + revenue +
                '}' + '\n';
    }
}
