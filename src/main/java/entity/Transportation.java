package entity;

import entity.enums.TransportationType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "transportation")
public class Transportation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "departure_address")
    private String departureAddress;

    @Column(name = "arrival_address")
    private String arrivalAddress;

    @Column(name = "date_departure", nullable = false)
    private LocalDate departureDate;

    @Column(name = "date_arrival", nullable = false)
    private LocalDate arrivalDate;

    @Min(value = 0L, message = "Price must be a positive number!")
    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private TransportationType type;

    @Min(value = 0, message = "Weight must be a positive number!")
    @Column(name = "weight")
    private Double weight;

    @ManyToMany(mappedBy = "transportations", targetEntity = Client.class)
    private Set<Client> clients;

    @ManyToOne
    @JoinColumn(name = "driver_id")
    private Driver driver;

    @ManyToOne(targetEntity = Company.class, fetch = FetchType.LAZY)
    private Company company;

    public Transportation(String departureAddress, String arrivalAddress, LocalDate departureDate, LocalDate arrivalDate, Double price, TransportationType type, Double weight, Set<Client> clients, Driver driver, Company company) {
        this.departureAddress = departureAddress;
        this.arrivalAddress = arrivalAddress;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.price = price;
        this.type = type;
        this.weight = weight;
        this.clients = clients;
        this.driver = driver;
        this.company = company;
    }

    public Transportation(String departureAddress, String arrivalAddress, LocalDate departureDate, LocalDate arrivalDate, Double price, TransportationType type, Set<Client> clients, Driver driver, Company company) {
        this.departureAddress = departureAddress;
        this.arrivalAddress = arrivalAddress;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.price = price;
        this.type = type;
        this.clients = clients;
        this.driver = driver;
        this.company = company;
    }

    public Transportation(String departureAddress, String arrivalAddress, LocalDate departureDate, LocalDate arrivalDate, Double price, TransportationType type) {
        this.departureAddress = departureAddress;
        this.arrivalAddress = arrivalAddress;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.price = price;
    }

    public Transportation(String departureAddress, String arrivalAddress, LocalDate departureDate, LocalDate arrivalDate, Double price, TransportationType type, Double weight) {
        this.departureAddress = departureAddress;
        this.arrivalAddress = arrivalAddress;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.price = price;
        this.type = type;
        this.weight = weight;
    }
}
