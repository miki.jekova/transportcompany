package entity;

import entity.enums.QualificationType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.util.Set;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="driver")
public class Driver {

    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Pattern(regexp = ".*([a-zA-Z])", message = "You must enter valid first name(containing only letters)!")
    @Column(name = "first_name")
    private String firstName;

    @Pattern(regexp = ".*([a-zA-Z])", message = "You must enter valid last name(containing only letters)!")
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "qualification")
    @Enumerated(EnumType.STRING)
    private QualificationType qualification;

    @Min(value = 0L, message = "Salary must be a positive number!")
    @Column(name = "salary")
    private double salary;

    @ManyToMany(targetEntity = Vehicle.class)
    private Set<Vehicle> vehicles;

    @OneToMany(mappedBy = "driver", cascade = CascadeType.ALL)
    private Set<Transportation> transportations;

    @ManyToOne(targetEntity = Company.class, fetch = FetchType.LAZY)
    private Company company;

    public Driver(String id, String firstName, String lastName, QualificationType qualification, double salary, Set<Vehicle> vehicles, Set<Transportation> transportations) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.qualification = qualification;
        this.salary = salary;
        this.vehicles = vehicles;
        this.transportations = transportations;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", qualification=" + qualification +
                ", salary=" + salary +
                '}' + '\n';
    }
}
