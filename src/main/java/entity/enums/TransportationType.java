package entity.enums;

public enum TransportationType {
    PASSENGERS, GOODS, SPECIAL
}
