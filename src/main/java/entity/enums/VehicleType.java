package entity.enums;

public enum VehicleType {
    BUS, TRUCK, SPECIAL
}
