package entity;

import entity.enums.VehicleType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "vehicle")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Size(min = 8, max = 8, message = "You must enter valid registration number(Bulgaria)!")
    @Column(name = "registration_number", nullable = false)
    private String registrationNumber;

    @Column(name = "model", nullable = false)
    private String model;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private VehicleType type;

    @ManyToOne(targetEntity = Company.class, fetch = FetchType.LAZY)
    private Company company;

    @ManyToMany(targetEntity = Driver.class)
    private Set<Driver> drivers;

    public Vehicle(String registrationNumber, String model, VehicleType type) {
        this.id = 1;
        this.registrationNumber = registrationNumber;
        this.model = model;
        this.type = type;
    }
}
