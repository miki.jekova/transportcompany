package dao;

import configuration.SessionFactoryUtil;
import entity.Driver;
import entity.Transportation;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DriverDAO {

    public static void saveDriver(Driver driver) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(driver);
        transaction.commit();
        session.close();
    }

    public static Driver getDriver(String id) {
        Driver driver;
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        driver = session.get(Driver.class, id);
        transaction.commit();
        return driver;
    }

    public static void updateDriver(Driver driver) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(driver);
        transaction.commit();
        session.close();
    }

    public static void deleteDriver(Driver driver) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(driver);
        transaction.commit();
        session.close();
    }

    public static void saveDrivers(Collection<Driver> driverSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        driverSet.forEach(session::save);
        transaction.commit();
        session.close();
    }

    public static List<Driver> getDrivers() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("SELECT d FROM Driver d", Driver.class).getResultList();
    }

    public static void updateDrivers(Collection<Driver> drivers) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        drivers.forEach(session::update);
        transaction.commit();
        session.close();
    }

    public static void deleteDrivers(Collection<Driver> drivers) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        drivers.forEach(session::delete);
        transaction.commit();
        session.close();
    }

    public static List<Driver> getDriversSortedBySalary() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Driver> criteriaQuery = criteriaBuilder.createQuery(Driver.class);
        Root<Driver> root = criteriaQuery.from(Driver.class);
        criteriaQuery.select(root).orderBy(criteriaBuilder.desc(root.get("salary")));
        return session.createQuery(criteriaQuery).getResultList();
    }

    public static List<Driver> getDriversSortedByQalificationAndSalary() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Driver> criteriaQuery = criteriaBuilder.createQuery(Driver.class);
        Root<Driver> root = criteriaQuery.from(Driver.class);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("qualification")), criteriaBuilder.asc(root.get("salary")));
        return session.createQuery(criteriaQuery).getResultList();
    }

    public static List<Transportation> getDriverTransportations(Driver d) {
        String id = d.getId();
        Driver driver;
        Session session = configuration.SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        driver = session.createQuery(
                "select d from Driver d" +
                        " join fetch d.transportations" +
                        " where d.id = :id",
                Driver.class)
                .setParameter("id", id)
                .getSingleResult();
        transaction.commit();
        return new ArrayList<>(driver.getTransportations());
    }
}