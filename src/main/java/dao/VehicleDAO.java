package dao;

import configuration.SessionFactoryUtil;
import entity.Vehicle;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collection;
import java.util.List;

public class VehicleDAO {

    public static void saveVehicle(Vehicle vehicle) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(vehicle);
        transaction.commit();
        session.close();
    }

    public static Vehicle getVehicle(Integer id) {
        Vehicle vehicle;
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        vehicle = session.get(Vehicle.class, id);
        transaction.commit();
        return vehicle;
    }

    public static void updateVehicle(Vehicle vehicle) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(vehicle);
        transaction.commit();
        session.close();
    }

    public static void deleteVehicle(Vehicle vehicle) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(vehicle);
        transaction.commit();
        session.close();
    }

    public static void saveVehicles(Collection<Vehicle> vehicles) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        vehicles.forEach(session::save);
        transaction.commit();
        session.close();
    }

    public static List<Vehicle> getVehicles() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("SELECT v FROM Vehicle v", Vehicle.class).getResultList();
    }

    public static void updateVehicles(Collection<Vehicle> vehicles) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        vehicles.forEach(session::update);
        transaction.commit();
        session.close();
    }

    public static void deleteVehicles(Collection<Vehicle> vehicles) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        vehicles.forEach(session::delete);
        transaction.commit();
        session.close();
    }
}