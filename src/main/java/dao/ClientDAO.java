package dao;


import configuration.SessionFactoryUtil;
import entity.Client;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collection;
import java.util.List;

public class ClientDAO {

    public static void saveClient(Client client) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(client);
        transaction.commit();
        session.close();
    }

    public static Client getClient(long id) {
        Client client;
        Session session = configuration.SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        client = session.get(Client.class, id);
        transaction.commit();
        return client;
    }

    public static void updateClient(Client client) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(client);
        transaction.commit();
        session.close();
    }

    public static void deleteClient(Client client) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(client);
        transaction.commit();
        session.close();
    }

    public static void saveClients(Collection<Client> clientSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        clientSet.forEach(session::save);
        transaction.commit();
        session.close();
    }

    public static List<Client> getClients() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("SELECT c FROM Client c", Client.class).getResultList();
    }

    public static void updateClients(Collection<Client> clientSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        clientSet.forEach(session::update);
        transaction.commit();
        session.close();
    }

    public static void deleteClients(Collection<Client> clientSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        clientSet.forEach(session::delete);
        transaction.commit();
        session.close();
    }
}
