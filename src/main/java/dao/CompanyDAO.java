package dao;

import configuration.SessionFactoryUtil;
import entity.Company;
import entity.Driver;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class CompanyDAO {

    public static void saveCompany(Company company) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(company);
        transaction.commit();
        session.close();
    }

    public static Company getCompany(String id) {
        Company company;
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        company = session.get(Company.class, id);
        transaction.commit();
        return company;
    }

    public static void updateCompany(Company company) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(company);
        transaction.commit();
        session.close();
    }

    public static void deleteCompany(Company company) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(company);
        transaction.commit();
        session.close();
    }

    public static void saveCompanies(Collection<Company> companyList) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        companyList.forEach(CompanyDAO::saveCompany);
        transaction.commit();
        session.close();
    }

    public static List<Company> getCompanies() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("SELECT c FROM Company c", Company.class).getResultList();
    }

    public static void updateCompanies(Collection<Company> companyList) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        companyList.forEach(CompanyDAO::updateCompany);
        transaction.commit();
        session.close();
    }

    public static void deleteCompanies(Collection<Company> companySet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        companySet.forEach(CompanyDAO::deleteCompany);
        session.delete(companySet);
        transaction.commit();
        session.close();
    }

    public static Set<Driver> getCompanyDrivers(long id) {
        Company company;
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        company = session.createQuery(
                "select c from Company c" +
                        " join fetch c.drivers" +
                        " where c.id = :id",
                Company.class)
                .setParameter("id", id)
                .getSingleResult();
        transaction.commit();

        return company.getDrivers();
    }

    public static List<Company> getCompaniesSortedByName() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Company> criteriaQuery = criteriaBuilder.createQuery(Company.class);
        Root<Company> root = criteriaQuery.from(Company.class);
        criteriaQuery.select(root).orderBy(criteriaBuilder.asc(root.get("name")));
        return session.createQuery(criteriaQuery).getResultList();
    }

    public static List<Company> getCompaniesSortedByRevenue() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Company> criteriaQuery = criteriaBuilder.createQuery(Company.class);
        Root<Company> root = criteriaQuery.from(Company.class);
        criteriaQuery.select(root).orderBy(criteriaBuilder.desc(root.get("revenue")));
        return session.createQuery(criteriaQuery).getResultList();
    }

    public static List<Company> getCompaniesWithName(String name) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Company> criteriaQuery = criteriaBuilder.createQuery(Company.class);
        Root<Company> root = criteriaQuery.from(Company.class);
        criteriaQuery.select(root).orderBy(criteriaBuilder.asc(root.get("name")));
        return session.createQuery(criteriaQuery).getResultList();
    }

    public static List<Company> getCompaniesWithRevenueGreaterThan(Double minRevenue) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Company> criteriaQuery = criteriaBuilder.createQuery(Company.class);
        Root<Company> root = criteriaQuery.from(Company.class);
        criteriaQuery.select(root).where(criteriaBuilder.greaterThan(root.get("revenue"), minRevenue));
        return session.createQuery(criteriaQuery).getResultList();
    }

    public static List<Company> getCompaniesWithRevenueLessThan(Double maxRevenue) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Company> criteriaQuery = criteriaBuilder.createQuery(Company.class);
        Root<Company> root = criteriaQuery.from(Company.class);
        criteriaQuery.select(root).where(criteriaBuilder.lessThan(root.get("revenue"), maxRevenue));
        return session.createQuery(criteriaQuery).getResultList();
    }

    public static List<Company> getCompaniesWithRevenueBetween(Double minRevenue, Double maxRevenue) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Company> criteriaQuery = criteriaBuilder.createQuery(Company.class);
        Root<Company> root = criteriaQuery.from(Company.class);
        criteriaQuery.select(root).where(criteriaBuilder.between(root.get("revenue"), minRevenue, maxRevenue));
        return session.createQuery(criteriaQuery).getResultList();
    }
}
