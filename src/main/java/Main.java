import dao.*;
import entity.*;
import entity.enums.QualificationType;
import entity.enums.TransportationType;
import entity.enums.VehicleType;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        /*System.out.println(DriverDAO.getDriversSortedBySalary());*/
        System.out.println("*************************************");
        System.out.println(CompanyDAO.getCompaniesSortedByRevenue());
        System.out.println("*************************************");
        System.out.println(CompanyDAO.getCompaniesSortedByName());
        System.out.println("*************************************");
        System.out.println(DriverDAO.getDriversSortedBySalary());
        System.out.println("*************************************");

        /*saveAllToDB();*/
        System.out.flush();
    }
    /* Populate data with saveAllToDB */
    private static void saveAllToDB() {
        Vehicle vehicle = new Vehicle("CB 1691 TM", "Kia Ceed", VehicleType.BUS);
        Vehicle vehicle2 = new Vehicle("P 0110 BH", "Citroen C3", VehicleType.TRUCK);
        Vehicle vehicle3 = new Vehicle("C 2HO5 OH", "WV Drunk", VehicleType.SPECIAL);

        HashSet<Vehicle> vehicles = new HashSet<>();
        vehicles.add(vehicle);
        vehicles.add(vehicle2);
        vehicles.add(vehicle3);

        VehicleDAO.saveVehicles(vehicles);

        Driver driver = new Driver("9101125397", "Radostina", "Zhekova", QualificationType.BUS, 5000, Collections.emptySet(), Collections.emptySet());
        Driver driver2 = new Driver("9804185314", "Mikaela", "Zhekova", QualificationType.TRUCK, 4000, Collections.emptySet(), Collections.emptySet());
        Driver driver3 = new Driver("9609125577", "Viktor", "Ivanov", QualificationType.SPECIAL, 8000, Collections.emptySet(), Collections.emptySet());

        Set<Driver> drivers = new HashSet<>();
        drivers.add(driver);
        drivers.add(driver2);
        drivers.add(driver3);

        DriverDAO.saveDrivers(drivers);

        Client client = new Client("6908234455", "Tatyana", "Zhekova", Collections.emptySet(), Collections.emptySet());
        Client client2 = new Client("6508310012", "Emil", "Zhekov", Collections.emptySet(), Collections.emptySet());
        Client client3 = new Client("84093136772", "Iskren", "Dimov", Collections.emptySet(), Collections.emptySet());

        Set<Client> clients = new HashSet<>();
        clients.add(client);
        clients.add(client2);
        clients.add(client3);

        ClientDAO.saveClients(clients);

        Company company = new Company("Deep Green", 1000000.0, drivers, vehicles, clients, Collections.emptySet());
        Company company2 = new Company("Cheezy Sisters",2000.0, drivers, vehicles, clients, Collections.emptySet());

        Set<Company> companies = new HashSet<>();
        companies.add(company);
        companies.add(company2);

        CompanyDAO.saveCompanies(companies);

        Transportation t = new Transportation("Ruse", "Sofia", LocalDate.now(), LocalDate.now(),100.0, TransportationType.GOODS, 400.0, Collections.emptySet(), driver, company);
        Transportation t2 = new Transportation("Troyan", "Sofia", LocalDate.now(), LocalDate.now(),200.0, TransportationType.GOODS, 5000.0, Collections.emptySet(), driver2, company);
        Transportation t3 = new Transportation("Shtraklevo", "Ruse", LocalDate.now(), LocalDate.now(), 10.0, TransportationType.PASSENGERS, Collections.emptySet(), driver3, company);
        Transportation t4 = new Transportation("Sitovo", "Silistra", LocalDate.now(), LocalDate.now(), 25.0, TransportationType.PASSENGERS, Collections.emptySet(), driver, company);
        HashSet<Transportation> transportations = new HashSet<>();
        transportations.add(t);
        transportations.add(t2);
        transportations.add(t3);
        transportations.add(t4);

        TransportationDAO.saveTransportation(t);
        TransportationDAO.saveTransportation(t2);

        company.setClients(clients);
        company.setDrivers(drivers);

        CompanyDAO.updateCompany(company);

        vehicle.setCompany(company);
        vehicle2.setCompany(company2);
        vehicle3.setCompany(company);

        VehicleDAO.updateVehicle(vehicle);
        VehicleDAO.updateVehicle(vehicle2);
        VehicleDAO.updateVehicle(vehicle3);

        driver.setCompany(company);
        driver2.setCompany(company2);
        driver3.setCompany(company);

        DriverDAO.updateDriver(driver);
        DriverDAO.updateDriver(driver2);
        DriverDAO.updateDriver(driver3);


        /*System.out.println(CompanyDAO.getCompanyDrivers(company.getId()));

        System.out.flush();*/
    }
}
